cmake_minimum_required(VERSION 3.26)

project(saltywitch)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING "" FORCE)
endif()

set(CMAKE_C_STANDARD 99)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_LINK_WHAT_YOU_USE ON)

if(CMAKE_C_COMPILER_FRONTEND_VARIANT STREQUAL "GNU")
    set(CMAKE_C_FLAGS "-O3 -shared ${CMAKE_C_FLAGS}")
endif()

check_ipo_supported(RESULT has_ipo OUTPUT error)
if(has_ipo)
    set(CMAKE_INTERPROCEDURAL_OPTIMIZATION_RELEASE ON)
endif()

set(SALTYWITCH_SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/saltywitch.c
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/atoms.c
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/auth.c
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/generichash.c
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/pwhash.c
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/saltywitch.c
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/saltywitch.h
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/secretbox.c
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src/shorthash.c
)

set(INCLUDE_DIRS
    ${ERTS_INCLUDE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/c_src
)

find_package(libsodium REQUIRED)

add_library(saltywitch SHARED ${SALTYWITCH_SOURCES})

target_compile_definitions(saltywitch PUBLIC "$<$<CONFIG:RELEASE>:NDEBUG>")
target_include_directories(${PROJECT_NAME} PUBLIC ${INCLUDE_DIRS})

target_link_libraries(saltywitch PRIVATE libsodium)
