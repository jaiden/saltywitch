# SaltyWitch

Another NIF library for libsodium.

## Roadmap

Easy cross-compiling the NIF.

## Building

On UNIX-like systems:
- Shell (tested on bash and busybox ash)
- Make
- A GCC-like C99 compiler.
- `meson` _(optional but **recommended**)_
- `pkgconf` *(if directly building)*

`SALTYWITCH_BUILD_SYSTEM`: By default `direct` is used for UNIX systems.
    - **meson** _(recommended)_: Use meson to build the NIF.
    - **cmake**: Use CMake to build the NIF
    - **direct**: Build the NIF in a single compiler invocation like a lunatic.

`SALTYWITCH_MESON_ARGS`: extra arguments for meson
    - do not specify `--prefix`

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `saltywitch` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:saltywitch, "~> 0.0.1"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/saltywitch>.
