# C Style used


- Macros are to be avoided.
- Any macro defined must be defined before its used and `undef`'d after done.

Exception part must be as following at bare minimum:


If there are more than 1 points of exception, they all must be deduplicated through `goto`'s.
The variables `err` and `reason` must be used with the following initial values.

```c
ERL_NIF_TERM err    = atom_badarg;
ERL_NIF_TERM reason = atom_err_opaque;
```

