#!/usr/bin/env sh

if [ -z $SALTYWITCH_BUILD_SYSTEM ]; then
    printf "[build.sh] trying to detect better build systems... "

#    if [ -x "$(command -v cmake)" ]; then
#        printf "cmake "
#        SALTYWITCH_BUILD_SYSTEM="cmake"
#    fi

    if [ -x "$(command -v meson)" ]; then
        printf "meson "
        SALTYWITCH_BUILD_SYSTEM="meson"
    fi

    printf "\n"
fi

case ${SALTYWITCH_BUILD_SYSTEM:-direct} in
    meson)
        echo "[build.sh] using meson to build nif"
        if ! [ -d "./build-meson" ]; then
            meson setup build-meson \
                --prefix=$MIX_APP_PATH \
                -Dmix_target=$MIX_TARGET \
                -Derts_include_dir=$ERTS_INCLUDE_DIR \
                -Derl_interface_lib_dir=$ERL_INTERFACE_LIB_DIR \
                -Derl_interface_include_dir=$ERL_INTERFACE_INCLUDE_DIR \
                $SALTYWITCH_MESON_ARGS
        fi

        meson compile -C build-meson
        meson install -C build-meson
    ;;

#    cmake)
#        echo "[build.sh] using cmake to build nif"
#    ;;

    direct)
        echo "[build.sh] directly building the nif"
        ${CC:-cc} c_src/*.c \
            -shared \
            ${CFLAGS:--O3 -fPIC -fstack-protector -fvisibility=hidden} \
            $LDFLAGS \
            -I $ERTS_INCLUDE_DIR \
            -I $ERL_EI_INCLUDE_DIR \
            -I $ERL_INTERFACE_INCLUDE_DIR \
            -L $ERL_EI_LIBDIR \
            -lei \
            $(pkgconf --libs --cflags libsodium) \
            -o $MIX_APP_PATH/priv/saltywitch.so
    ;;

    *)
        echo "[build.sh] invalid build backend"
        exit 1
    ;;
esac
