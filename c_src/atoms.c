#include "saltywitch.h"

ERL_NIF_TERM atom_badarg;
ERL_NIF_TERM atom_error;
ERL_NIF_TERM atom_ok;

ERL_NIF_TERM atom_err_opaque;

ERL_NIF_TERM atom_err_invalid_type;
ERL_NIF_TERM atom_err_nif_alloc;

ERL_NIF_TERM atom_err_invalid_key_size;
ERL_NIF_TERM atom_err_invalid_nonce_size;
ERL_NIF_TERM atom_err_invalid_salt_size;
ERL_NIF_TERM atom_err_invalid_seed_size;
ERL_NIF_TERM atom_err_invalid_tag_size;
ERL_NIF_TERM atom_err_verification_failed;

ERL_NIF_TERM atom_err_ciphertext_too_small;
ERL_NIF_TERM atom_err_key_too_large;
ERL_NIF_TERM atom_err_key_too_small;
ERL_NIF_TERM atom_err_output_too_large;
ERL_NIF_TERM atom_err_output_too_small;

ERL_NIF_TERM atom_err_pwhash_too_long;
ERL_NIF_TERM atom_err_pwhash_needs_rehash;
ERL_NIF_TERM atom_err_pwhash_mem_too_large;
ERL_NIF_TERM atom_err_pwhash_mem_too_small;
ERL_NIF_TERM atom_err_pwhash_ops_too_large;
ERL_NIF_TERM atom_err_pwhash_ops_too_small;

#define DEFERROR(name) atom_err_##name = enif_make_atom(env, #name)

void saltywitch_nif_init_atoms(ErlNifEnv *env)
{
	atom_error  = enif_make_atom(env, "error");
	atom_badarg = enif_make_atom(env, "badarg");
	atom_ok     = enif_make_atom(env, "ok");

	DEFERROR(opaque);

	DEFERROR(invalid_type);
	DEFERROR(nif_alloc);

	DEFERROR(invalid_key_size);
	DEFERROR(invalid_nonce_size);
	DEFERROR(invalid_salt_size);
	DEFERROR(invalid_seed_size);
	DEFERROR(invalid_tag_size);
	DEFERROR(verification_failed);

	DEFERROR(ciphertext_too_small);
	DEFERROR(key_too_large);
	DEFERROR(key_too_small);
	DEFERROR(output_too_large);
	DEFERROR(output_too_small);

	DEFERROR(pwhash_too_long);
	DEFERROR(pwhash_needs_rehash);
	DEFERROR(pwhash_mem_too_large);
	DEFERROR(pwhash_mem_too_small);
	DEFERROR(pwhash_ops_too_large);
	DEFERROR(pwhash_ops_too_small);
}

#undef DEFERROR
