#include "saltywitch.h"

/* auth */
ERL_NIF_TERM saltywitch_auth_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	ERL_NIF_TERM term;

	unsigned char *key = enif_make_new_binary(env, crypto_auth_KEYBYTES, &term);
	if (key == NULL) {
		return saltywitch_exception(env, atom_error, atom_err_nif_alloc);
	}

	crypto_auth_keygen(key);

	return term;
}

ERL_NIF_TERM saltywitch_auth(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	ErlNifBinary message, key;
	ERL_NIF_TERM term;

	unsigned char *mac;

	ERL_NIF_TERM err    = atom_badarg;
	ERL_NIF_TERM reason = atom_err_opaque;

	if (enif_inspect_binary(env, argv[0], &message) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (enif_inspect_binary(env, argv[1], &key) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (key.size != crypto_auth_KEYBYTES) {
		reason = atom_err_invalid_key_size;
		goto badarg_FAULT;
	}

	mac = enif_make_new_binary(env, crypto_auth_BYTES, &term);
	if (mac == NULL) {
		reason = atom_err_nif_alloc;
		goto error_FAULT;
	}

	crypto_auth(mac, message.data, message.size, key.data);

	return term;

error_FAULT:
	err = atom_error;
badarg_FAULT:
	return saltywitch_exception(env, err, reason);
}

ERL_NIF_TERM saltywitch_auth_verify(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	ErlNifBinary tag, message, key;

	ERL_NIF_TERM err    = atom_badarg;
	ERL_NIF_TERM reason = atom_err_opaque;

	if (enif_inspect_binary(env, argv[0], &tag) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (tag.size != crypto_auth_BYTES) {
		reason = atom_err_invalid_tag_size;
		goto badarg_FAULT;
	}

	if (enif_inspect_binary(env, argv[1], &message) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (enif_inspect_binary(env, argv[2], &key) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (key.size != crypto_auth_KEYBYTES) {
		reason = atom_err_invalid_key_size;
		goto badarg_FAULT;
	}

	if (crypto_auth_verify(tag.data, message.data, message.size, key.data) != 0) {
		reason = atom_err_verification_failed;
		goto error_FAULT;
	}

	return atom_ok;

error_FAULT:
	err = atom_error;
badarg_FAULT:
	return saltywitch_exception(env, err, reason);
}
