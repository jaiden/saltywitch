#include "saltywitch.h"

_Static_assert(sizeof(unsigned int) >= sizeof(uint32_t), "unsigned int must be greated than 32 bits");

ERL_NIF_TERM saltywitch_randombytes_random(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	(void) argc;
	(void) argv;

	return enif_make_uint(env, randombytes_random());
}

ERL_NIF_TERM saltywitch_randombytes_uniform(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	uint32_t upper;

	(void) argc;

	if (enif_get_uint(env, argv[0], &upper) == false) {
		return saltywitch_exception(env, atom_badarg, atom_err_invalid_type);
	}

	return enif_make_uint(env, randombytes_uniform(upper));
}

ERL_NIF_TERM saltywitch_randombytes_buf(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	unsigned char *buf;
	ERL_NIF_TERM term;
	uint64_t len;

	(void) argc;

	if (enif_get_uint64(env, argv[0], &len) == false) {
		return saltywitch_exception(env, atom_badarg, atom_err_invalid_type);
	}

	buf = enif_make_new_binary(env, len, &term);
	if (buf == NULL) {
		return saltywitch_exception(env, atom_error, atom_err_nif_alloc);
	}

	randombytes_buf(buf, len);

	return term;
}

ERL_NIF_TERM saltywitch_randombytes_buf_deterministic(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	unsigned char *buf;
	ErlNifBinary seed;
	ERL_NIF_TERM term;
	uint64_t len;

	ERL_NIF_TERM err    = atom_badarg;
	ERL_NIF_TERM reason = atom_err_opaque;

	(void) argc;

	if (enif_get_uint64(env, argv[0], &len) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (enif_inspect_binary(env, argv[1], &seed) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (seed.size != randombytes_SEEDBYTES) {
		reason = atom_err_invalid_seed_size;
		goto badarg_FAULT;
	}

	buf = enif_make_new_binary(env, len, &term);
	if (buf == NULL) {
		reason = atom_err_nif_alloc;
		goto error_FAULT;
	}

	randombytes_buf_deterministic(buf, len, seed.data);

	return term;

error_FAULT:
	err = atom_error;
badarg_FAULT:
	return saltywitch_exception(env, err, reason);
}

ERL_NIF_TERM saltywitch_randombytes_seedbytes(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	(void) argc;
	(void) argv;

	return enif_make_uint64(env, randombytes_SEEDBYTES);
}
