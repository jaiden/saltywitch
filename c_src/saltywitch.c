#include "saltywitch.h"

_Static_assert((sizeof(size_t) >= sizeof(uint64_t)), "size_t must be at least 64-bits");

ERL_NIF_TERM saltywitch_exception(ErlNifEnv *env, ERL_NIF_TERM type, ERL_NIF_TERM reason)
{
	ERL_NIF_TERM exception = enif_make_tuple2(env, type, reason);

	return enif_raise_exception(env, exception);
}

static int saltywitch_load(ErlNifEnv *env, void **data, ERL_NIF_TERM info)
{
	(void) data;
	(void) info;

	if (sodium_init() < 0) {
		return -1;
	}

	saltywitch_nif_init_atoms(env);

	if (saltywitch_nif_init_generichash(env) != 0) {
		return -1;
	}

	return 0;
}

static ERL_NIF_TERM saltywitch_info(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	(void) argc;
	(void) argv;

	return enif_make_tuple3(env,
	                        enif_make_string(env, SODIUM_VERSION_STRING, ERL_NIF_LATIN1),
	                        enif_make_int(env, SODIUM_LIBRARY_VERSION_MAJOR),
	                        enif_make_int(env, SODIUM_LIBRARY_VERSION_MINOR));
}

static ErlNifFunc nif_funcs[] = {
    {"info", 0, saltywitch_info},

    /* random data */
    {"randombytes_random", 0, saltywitch_randombytes_random},
    {"randombytes_uniform", 1, saltywitch_randombytes_uniform},
    {"randombytes_buf", 1, saltywitch_randombytes_buf},
    {"randombytes_buf_deterministic", 2, saltywitch_randombytes_buf_deterministic},
    {"randombytes_seedbytes", 0, saltywitch_randombytes_seedbytes},

    /* secretstream */
    /*
    {"secretstream_xchacha20poly1305_keygen", 0, saltywitch_secretstream_xchacha20poly1305_keygen},
    {"secretstream_xchacha20poly1305_init_push", 1, saltywitch_secretstream_xchacha20poly1305_init_push},
    */

    /* secretbox */
    {"secretbox_keygen", 0, saltywitch_secretbox_keygen},

    {"secretbox_easy", 3, saltywitch_secretbox_easy},
    {"secretbox_open_easy", 3, saltywitch_secretbox_open_easy},

    {"secretbox_detached", 3, saltywitch_secretbox_detached},
    {"secretbox_open_detached", 4, saltywitch_secretbox_open_detached},

    {"secretbox_keybytes", 0, saltywitch_secretbox_keybytes},
    {"secretbox_macbytes", 0, saltywitch_secretbox_macbytes},
    {"secretbox_noncebytes", 0, saltywitch_secretbox_noncebytes},

    /* generichash */
    {"generichash", 1, saltywitch_generichash},
    {"generichash", 2, saltywitch_generichash},

    {"generichash_keygen", 0, saltywitch_generichash_keygen},

    {"generichash_init", 0, saltywitch_generichash_init},
    {"generichash_init", 1, saltywitch_generichash_init},
    {"generichash_init", 2, saltywitch_generichash_init},
    {"generichash_update", 2, saltywitch_generichash_update, ERL_NIF_DIRTY_JOB_CPU_BOUND},
    {"generichash_final", 1, saltywitch_generichash_final},

    /* shorthash */
    {"shorthash_keygen", 0, saltywitch_shorthash_keygen},
    {"shorthash", 2, saltywitch_shorthash},

    /* pwhash */
    //{"pwhash", 6, saltywitch_pwhash, ERL_NIF_DIRTY_JOB_CPU_BOUND},
    {"pwhash_str", 3, saltywitch_pwhash_str, ERL_NIF_DIRTY_JOB_CPU_BOUND},
    {"pwhash_str_verify", 2, saltywitch_pwhash_str_verify, ERL_NIF_DIRTY_JOB_CPU_BOUND},
    {"pwhash_str_needs_rehash", 3, saltywitch_pwhash_str_needs_rehash},
};

ERL_NIF_INIT(Elixir.SaltyWitch.NIF, nif_funcs, saltywitch_load, NULL, NULL, NULL)
