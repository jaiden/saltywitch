#pragma once

#include <erl_nif.h>
#include <sodium.h>
#include <stdint.h>

#if __STDC_VERSION__ < 202300L
#include <stdbool.h>
#endif

extern ERL_NIF_TERM atom_badarg;
extern ERL_NIF_TERM atom_error;
extern ERL_NIF_TERM atom_ok;

extern ERL_NIF_TERM atom_err_opaque;

extern ERL_NIF_TERM atom_err_invalid_type;
extern ERL_NIF_TERM atom_err_nif_alloc;

extern ERL_NIF_TERM atom_err_invalid_key_size;
extern ERL_NIF_TERM atom_err_invalid_nonce_size;
extern ERL_NIF_TERM atom_err_invalid_salt_size;
extern ERL_NIF_TERM atom_err_invalid_seed_size;
extern ERL_NIF_TERM atom_err_invalid_tag_size;
extern ERL_NIF_TERM atom_err_verification_failed;

extern ERL_NIF_TERM atom_err_ciphertext_too_small;
extern ERL_NIF_TERM atom_err_key_too_large;
extern ERL_NIF_TERM atom_err_key_too_small;
extern ERL_NIF_TERM atom_err_output_too_large;
extern ERL_NIF_TERM atom_err_output_too_small;

extern ERL_NIF_TERM atom_err_pwhash_too_long;
extern ERL_NIF_TERM atom_err_pwhash_needs_rehash;
extern ERL_NIF_TERM atom_err_pwhash_mem_too_large;
extern ERL_NIF_TERM atom_err_pwhash_mem_too_small;
extern ERL_NIF_TERM atom_err_pwhash_ops_too_large;
extern ERL_NIF_TERM atom_err_pwhash_ops_too_small;

/* init */
void saltywitch_nif_init_atoms(ErlNifEnv *env);
int saltywitch_nif_init_generichash(ErlNifEnv *env);

/* helpers */
ERL_NIF_TERM saltywitch_exception(ErlNifEnv *env, ERL_NIF_TERM type, ERL_NIF_TERM reason);

/* random */
ERL_NIF_TERM saltywitch_randombytes_random(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_randombytes_uniform(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_randombytes_buf(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_randombytes_buf_deterministic(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_randombytes_seedbytes(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

/* generichash */
ERL_NIF_TERM saltywitch_generichash(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_generichash_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_generichash_init(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_generichash_update(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_generichash_final(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

/* secretbox*/
ERL_NIF_TERM saltywitch_secretbox_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_secretbox_easy(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_secretbox_open_easy(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_secretbox_detached(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_secretbox_open_detached(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_secretbox_keybytes(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_secretbox_macbytes(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_secretbox_noncebytes(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

/* auth */
ERL_NIF_TERM saltywitch_auth_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth_verify(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_auth_hmacsha256_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth_hmacsha256(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth_hmacsha256_verify(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_auth_hmacsha512_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth_hmacsha512(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth_hmacsha512_verify(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM saltywitch_auth_hmacsha512256_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth_hmacsha512256(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_auth_hmacsha512256_verify(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

/* pwhash */
ERL_NIF_TERM saltywitch_pwhash(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_pwhash_str(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_pwhash_str_verify(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_pwhash_str_needs_rehash(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

/* shorthash */
ERL_NIF_TERM saltywitch_shorthash_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_shorthash(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);

/* key exchange */
/*
ERL_NIF_TERM saltywitch_kx_keypair(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_kx_seed_keypair(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_kx_client_session_keys(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_kx_server_session_keys(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);


ERL_NIF_TERM saltywitch_kx_publickeybytes(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM saltywitch_kx_publickeybytes(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]);
*/
