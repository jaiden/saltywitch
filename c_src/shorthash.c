#include "saltywitch.h"

ERL_NIF_TERM saltywitch_shorthash_keygen(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	ERL_NIF_TERM term;

	(void) argc;
	(void) argv;

	unsigned char *key = enif_make_new_binary(env, crypto_shorthash_KEYBYTES, &term);
	if (key == NULL) {
		return saltywitch_exception(env, atom_error, atom_err_opaque);
	}

	crypto_shorthash_keygen(key);

	return term;
}

ERL_NIF_TERM saltywitch_shorthash(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	ErlNifBinary key, message;
	unsigned char *hash;
	ERL_NIF_TERM term;

	ERL_NIF_TERM err    = atom_badarg;
	ERL_NIF_TERM reason = atom_err_opaque;

	(void) argc;

	if (enif_inspect_binary(env, argv[0], &key) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	if (key.size != crypto_shorthash_KEYBYTES) {
		reason = atom_err_invalid_key_size;
		goto badarg_FAULT;
	}

	if (enif_inspect_binary(env, argv[1], &message) == false) {
		reason = atom_err_invalid_type;
		goto badarg_FAULT;
	}

	hash = enif_make_new_binary(env, crypto_shorthash_BYTES, &term);
	if (hash == NULL) {
		reason = atom_err_nif_alloc;
		goto error_FAULT;
	}

	crypto_shorthash(hash, message.data, message.size, key.data);

	return term;

error_FAULT:
	err = atom_error;
badarg_FAULT:
	return saltywitch_exception(env, err, reason);
}
