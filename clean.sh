#!/usr/bin/env sh

for dir in ./build-*; do
    [ -d $dir ] && rm -r $dir
done
