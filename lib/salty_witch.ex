defmodule SaltyWitch do

  @moduledoc """
  Documentation for `SaltyWitch`.
  """

  @doc """
  Returns version information

    iex> SaltyWitch.info
    {'1.0.18', 10, 3}

  """

  def info do
    SaltyWitch.NIF.info()
  end
end
