defmodule SaltyWitch.NIF do
  @compile {:autoload, false}
  @on_load :__on_load__

  def __on_load__() do
    path = :filename.join(:code.priv_dir(:saltywitch), ~c"saltywitch")
    :erlang.load_nif(path, 0)
  end

  def info, do: :erlang.nif_error(:not_loaded)

  def randombytes_random, do: :erlang.nif_error(:not_loaded)

  # random
  def randombytes_random, do: :erlang.nif_error(:not_loaded)

  def randombytes_uniform(upper)
  def randombytes_uniform(_), do: :erlang.nif_error(:not_loaded)

  def randombytes_buf(len)
  def randombytes_buf(_), do: :erlang.nif_error(:not_loaded)

  def randombytes_buf_deterministic(len, seed)
  def randombytes_buf_deterministic(_, _), do: :erlang.nif_error(:not_loaded)

  def randombytes_seedbytes, do: :erlang.nif_error(:not_loaded)

  # secretbox
  def secretbox_keygen, do: :erlang.nif_error(:not_loaded)

  def secretbox_easy(message, nonce, key)
  def secretbox_easy(_, _, _), do: :erlang.nif_error(:not_loaded)

  def secretbox_open_easy(ciphertext, nonce, key)
  def secretbox_open_easy(_, _, _), do: :erlang.nif_error(:not_loaded)

  def secretbox_detached(message, nonce, key)
  def secretbox_detached(_, _, _), do: :erlang.nif_error(:not_loaded)

  def secretbox_open_detached(ciphertext, mac, nonce, key)
  def secretbox_open_detached(_, _, _, _), do: :erlang.nif_error(:not_loaded)

  def secretbox_keybytes, do: :erlang.nif_error(:not_loaded)
  def secretbox_macbytes, do: :erlang.nif_error(:not_loaded)
  def secretbox_noncebytes, do: :erlang.nif_error(:not_loaded)

  # generichash
  def generichash(_), do: :erlang.nif_error(:not_loaded)
  def generichash(_, _), do: :erlang.nif_error(:not_loaded)

  def generichash_keygen, do: :erlang.nif_error(:not_loaded)

  def generichash_init, do: :erlang.nif_error(:not_loaded)
  def generichash_init(_), do: :erlang.nif_error(:not_loaded)
  def generichash_init(_, _), do: :erlang.nif_error(:not_loaded)
  def generichash_update(_, _), do: :erlang.nif_error(:not_loaded)
  def generichash_final(_), do: :erlang.nif_error(:not_loaded)

  # shorthash
  def shorthash_keygen, do: :erlang.nif_error(:not_loaded)

  def shorthash(key, message)
  def shorthash(_, _), do: :erlang.nif_error(:not_loaded)

  # pwhash
  def pwhash_str(pass, ops, mem)
  def pwhash_str(_, _, _), do: :erlang.nif_error(:not_loaded)

  def pwhash_str_verify(pwhash, pass)
  def pwhash_str_verify(_, _), do: :erlang.nif_error(:not_loaded)

  def pwhash_str_needs_rehash(pwhash, ops, mem)
  def pwhash_str_needs_rehash(_, _, _), do: :erlang.nif_error(:not_loaded)
end
