defmodule SaltyWitch.PasswordHash do
  alias SaltyWitch.NIF, as: N

  def string(pass, ops, mem) do
    N.pwhash_str(pass, ops, mem)
  end

  def verify(pwhash, pass) do
    N.pwhash_str_verify(pwhash, pass)
  end
end
