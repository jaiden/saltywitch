defmodule SaltyWitch.Random do
  alias SaltyWitch.NIF, as: N

  def seedbytes do
    N.randombytes_seedbytes()
  end

  def uint32 do
    N.randombytes_random()
  end

  def bytes(size) when size > 0 do
    N.randombytes_buf(size)
  end

  def deterministic(size, seed) when size > 0 do
    N.randombytes_buf_deterministic(size, seed)
  end
end
