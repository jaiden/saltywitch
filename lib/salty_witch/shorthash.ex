defmodule SaltyWitch.ShortHash do
  def keygen do
    SaltyWitch.NIF.shorthash_keygen()
  end

  def shorthash(key, message) do
    SaltyWitch.NIF.shorthash(key, message)
  end
end
