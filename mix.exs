defmodule SaltyWitch.MixProject do
  use Mix.Project

  @version "0.0.1"

  def project do
    [
      app: :saltywitch,
      version: @version,
      elixir: "~> 1.14",
      compilers: [:elixir_make] ++ Mix.compilers(),
      make_clean: ["clean"],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  defp deps do
    [
      {:elixir_make, "~> 0.7", runtime: false}
    ]
  end

  defp package do
    [
      maintainers: ["Aydin Mercan"],
      licenses: ["BSD-3-Clause"],
      files: [
        # Sources
        "c_src",
        "lib",
        "mix.exs",
        # Build
        "CMakeLists.txt",
        "Makefile",
        "build.sh",
        "clean.sh",
        "meson.build",
        # Meta
        "LICENSE",
        "README.md"
      ]
    ]
  end
end
